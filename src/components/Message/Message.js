import React from 'react';
import './Message.css';

const Message = ({author, message}) => {
    return (
        <p className="message-paragraph">
            <span className="author">{author} said:</span>
            <span className="message">{message}</span>
        </p>
    );
};

export default Message;
import React from 'react';

const Button = props => {
    let baseClass = ['button'];
    if (props.additionalClasses) {
        baseClass = baseClass.concat(props.additionalClasses);
    }

    return (
        <button
            className={baseClass.join(' ')}
            type="button"
            onClick={props.clicked}
        >
            {props.title}
        </button>
    );
};

export default Button;
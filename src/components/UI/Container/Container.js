import React from 'react';
import './Container.css';

const Container = props => {
    let baseCssClass = ['Container'];
    if (props.additionalClasses) {
        baseCssClass = baseCssClass.concat(props.additionalClasses);
    }
    return (
        <div className={baseCssClass.join(' ')}>
            {props.children}
        </div>
    );
};

export default Container;
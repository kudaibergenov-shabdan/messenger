import React, {useState} from 'react';
import './MessageSender.css';
import Container from "../UI/Container/Container";
import Button from "../UI/Button/Button";
import axios from "axios";

const MessageSender = () => {
    const url = 'http://146.185.154.90:8000/messages';
    const [message, setMessage] = useState();

    const messageChanged = (message) => {
        setMessage(message);
    }

    const sendMessage = () => {
        const sendData = async () => {
            try {
                const data = new URLSearchParams();
                data.set('message', message);
                data.set('author', 'Lermontov');
                await axios.post(url, data);
            } catch (e) {
                console.log('MyError' + e);
            }
        };
        sendData();
    };

    return (
        <Container additionalClasses={['MessageSender']}>
            <textarea
                className="message-textarea"
                onChange={e => messageChanged(e.target.value)}/>
            <Button
                title="Send"
                additionalClasses={['send-button']}
                clicked={sendMessage}
            />
        </Container>
    );
};

export default MessageSender;
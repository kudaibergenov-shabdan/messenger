import Container from "../UI/Container/Container";
import axios from "axios";
import {useEffect, useState} from "react";
import Message from "../Message/Message";

const MessageContainer = () => {

    const url = 'http://146.185.154.90:8000/messages';
    const [messages, setMessages] = useState([]);

    useEffect(() => {
        const fetchData = async (datetime) => {
            let response;
            if (!datetime) {
                response = await axios.get(url);
                setMessages(response.data);
            } else {
                response = await axios.get(url, {params: {datetime}});
                setMessages(messages => [...messages].concat(response.data));
            }
            if (response.data.length > 0) {
                return response.data[response.data.length - 1].datetime;
            }
        };

        fetchData()
            .then((lastMessageDateTime) => {
                setInterval(
                    () => {
                        fetchData(lastMessageDateTime)
                            .then((resolve) => {
                                lastMessageDateTime = resolve;
                            });
                    }, 5000);
            })
            .catch(e => console.error(e));
    }, [])

    return (
        <Container>
            {messages.map(messageObj => (
                <Message key={messageObj._id} author={messageObj.author} message={messageObj.message}/>
            )).reverse()}
        </Container>
    );
};

export default MessageContainer;
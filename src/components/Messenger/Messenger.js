import React from 'react';
import MessageSender from "../MessageSender/MessageSender";
import MessageContainer from "../MessageContainer/MessageContainer";

const Messenger = () => {
    return (
        <>
            <MessageSender/>
            <MessageContainer/>
        </>
    );
};

export default Messenger;
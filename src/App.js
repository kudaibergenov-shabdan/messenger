import Messenger from "./components/Messenger/Messenger";

const App = () => (
    <div className="App">
        <Messenger />
    </div>
);

export default App;
